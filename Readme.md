# variety-evernote-quotes
Evernotes quotes plugin for Linux Variety wallpaper changer (https://github.com/varietywalls/variety). You can add quotes to your desktop wallpaper with this plugin.

# Installation
- Install Variety (must be newer Python3 version, ex. from PPA ppa:peterlevi/ppa)
- Copy evernotes_quotes.py and settings_template.py files into ~/.config/variety/plugins  
- Rename settings_template.py to settings.py and add your Evernote developer token (go to: https://www.evernote.com/api/DeveloperToken.action, you may need to contact support to enable a production token at: https://www.evernote.com/SupportLogin.action)
- Run pip3 install -r requirements.txt (you may need to also install 'sudo apt install python3-testresources')
- View debug information at '/var/tmp/variety.EvernoteVarietyPlugin.log' or other configured location by setting DEBUG=True in settings.py
- Restart Variety (to view output information, start from the command line)
- Enable quotes by selecting the checkbox in Variety Preferences > Effects > Quotes > "Show random wise quotes on the desktop"
- Activate the plugin (named Evernote) from Variety Preferences > Effects > Quotes > Sources and filtering
- Add quotes to your Evernote Quotes notebook (or other notebook set in settings.py) with the quote first and the author's name after '--' (ex., "He who never makes an effort, never risks a failure. --Anonymous")
- If the font text is garbled on the desktop, make sure a valid font is selected in Variety Preferences > Effects > Quotes > Appearance > Text font


License

MIT — Copyright (c) 2020 Clifford B. Brown

# Build/Debug
## Set up virtual env: https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/
- cd to directory
- python3 -m pip install --user --upgrade pip
- python3 -m pip install --user virtualenv
- python3 -m venv env
- source env/bin/activate
- pip3 install -r requirements.txt
## Alternative to pip evernote3 package, add Evernote SDK for Python 3 from git
- git init
- git submodule add git://github.com/evernote/evernote-sdk-python3/ evernote
- git submodule init
- git submodule update
## Enable system packages to allow import of variety packages
- Update env/pyvenv.cfg to include-system-site-packages = true
- Update settings.py with Evernote token: EVERNOTE_TOKEN="xxx" and Evernote username: EVERNOTE_USERNAME="xxx"
## Run test
- python test.py
