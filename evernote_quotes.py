import os, re, random, json, time, threading, logging
from gettext import gettext
from html.parser import HTMLParser
import settings
# http://peterlevi.com/variety/wiki/Writing_plugins
# you must install Variety first for these packages to become available
from variety.plugins.IQuoteSource import IQuoteSource
# pip3 install git+git://github.com/evernote/evernote-sdk-python3.git#egg=evernote-sdk-python3
# https://dev.evernote.com/doc/start/python.php
from evernote.api.client import *
from evernote.edam.notestore import *
from evernote.edam.notestore.ttypes import *


logger = logging.getLogger("variety.EvernoteVarietyPlugin")
logFile = settings.LOG_FILE or '/var/tmp/variety.EvernoteVarietyPlugin.log'
if settings.DEBUG:
    hdlr = logging.FileHandler(logFile)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr) 
    logger.setLevel(logging.DEBUG)


quotes_notebook_name = settings.QUOTES_NOTEBOOK_NAME or "Quotes"
token = settings.EVERNOTE_TOKEN
sandbox = settings.SANDBOX or False
# Evernote link structure -> https://[service]/shard/[shardId]/nl/[userId]/[noteGuid]/
evernote_link_url_format = "https://{}.evernote.com".format("sandbox" if sandbox else "www") + "/shard/{}/nl/{}/{}/"
author_delimiters = settings.AUTHOR_DELIMITERS or ['--', '―']
refresh_seconds = settings.REFRESH_SECONDS or 3600 
refresh_seconds = refresh_seconds if refresh_seconds and refresh_seconds > 1800 else 1800

EVERNOTE_REFRESH_THREAD = "EVERNOTE_REFRESH_THREAD"


class HTMLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_text(self):
        return ' '.join(self.fed)

def strip_tags(html):
    s = HTMLStripper()
    s.feed(html)
    return s.get_text()


def truncate_ellipses(str, max):
    if str and max and len(str) > max:
        parts = str[:max-3].strip().rpartition(' ')
        if parts[0] == parts[1] == "":
            return str[:(max - 3)]+"..."    
        return parts[0]+"..."   
    return str 


# https://github.com/evernote/evernote-sdk-python3/blob/master/sample/client/EDAMTest.py
class EvernoteVarietyPlugin(IQuoteSource):

    def __init__(self):
        super(IQuoteSource, self).__init__()
        self.quotes = []
        if not token:
            error = "Evernote token not set in settings.py"
            logger.error(error)
            ValueError(error)


    @classmethod
    def get_info(cls):
        return {
            "name": "Evernote",
            "description": gettext("Displays quotes, defined in your Evernote notebook named Quotes.\n"
                             "The note format is:\n\n\"quote --author\"\n\nfor each note in the notebook...\n\n"
                             "You must put your Evernote developer token in the settings.py file.\n"),
            "author": "Clifford B. Brown",
            "version": "0.1"
        }


    def supports_search(self):
        return True


    def activate(self):
        if self.active:
            return
        logger.debug('Activating...')

        self.max_quote_len = self.jumble.parent.options.quotes_max_length - 1
        logger.debug('Max quote length set to: %s' % self.max_quote_len)

        super(EvernoteVarietyPlugin, self).activate()

        self.active = True

        def refresh_task():
            if self.active:
                try:
                    self.refresh()
                except Exception:
                    logger.exception("*****Refresh failed****")
                thread = threading.Timer(refresh_seconds, refresh_task)
                thread.setName(EVERNOTE_REFRESH_THREAD)
                thread.setDaemon(True)
                thread.start()            
        refresh_task()


    def deactivate(self):
        logger.debug('Deactivating.')
        self.active = False
        self.quotes = []
        client = None

        threads = [thread for thread in threading.enumerate() if thread != threading.main_thread() and thread.name == EVERNOTE_REFRESH_THREAD]
        for thread in threads:
            thread.cancel()


    def refresh(self):
        if self.active:
            logger.debug('Refreshing from Evernote at %s...' % time.ctime())
            self.connect_and_load()


    def get_evernote_client(self):
        return EvernoteClient(token=token, sandbox=sandbox, china=False)            


    def connect_and_load(self):
        client = self.get_evernote_client()

        userstore = client.get_user_store()
        user = userstore.getUser()
        userid = user.id
        shardid = user.shardId
        logger.debug("user: %s" % user.username)

        note_store = client.get_note_store()
        notebooks = note_store.listNotebooks()
        quotes_notebook = next((notebook for notebook in notebooks if notebook.name.lower() == quotes_notebook_name.lower()), None)
        if not quotes_notebook:
            error = "Your Evernote account has no notebooks with the name \"" + quotes_notebook_name + "\", as set up in settings.py or default"
            logger.error(error)
            ValueError(error)
        logger.debug("notebook: %s" % quotes_notebook)
        
        quotesMetaList = note_store.findNotesMetadata(token, NoteFilter(notebookGuid=quotes_notebook.guid), 0, 100, NotesMetadataResultSpec())
        if not quotesMetaList.notes:
            error = "Your Evernote account has no notes in the \"" + quotes_notebook_name + "\", notebook"
            logger.error(error)
            ValueError(error)

        self.load(user, note_store, quotesMetaList.notes)


    def load(self, user, note_store, notes):
        self.quotes = []
        try:
            logger.info("Loading quotes from Evernote...")
            for quoteMeta in notes:
                try:
                    guid = quoteMeta.guid
                    note = note_store.getNote(token, guid, True, True, False, False)   
                    fullQuote = ' '.join(strip_tags(note.content).replace('\n', ' ').split())         
                    logger.debug("Quote: %s" % fullQuote.encode("utf-8"))
                    parts = self.try_splits(fullQuote)
                    quote = ' '.join(parts[0].split())
                    if quote[0] == quote[-1] == '"':
                        quote = u"\u201C%s\u201D" % quote[1:-1]
                    author = parts[1].strip() if len(parts) > 1 else None

                    link = evernote_link_url_format.format(user.shardId, user.id, guid)
                    self.quotes.append({"quote": truncate_ellipses(quote, self.max_quote_len), "author": author, 
                                            "sourceURL": link, "link": link, "sourceName": "Evernote"})
                except Exception:
                    logger.debug('Could not process Evernote quote %s' % fullQuote)
        except Exception:
            logger.exception("Could not load quotes from Evernote")


    def try_splits(self, quote):
        partsRet = [quote]
        for delimiter in author_delimiters:
            parts = quote.rpartition(delimiter)
            if parts[0] == parts[1] == "":
                continue
            partsRet = [parts[0], parts[2]]
            break
        return partsRet


    def get_random(self):
        logger.debug("Loaded quotes count: %d" % len(self.quotes))
        logger.debug('Getting random quote...')
        if self.quotes:
            return [random.choice(self.quotes)]
        else:
            return self.quotes


    def get_for_author(self, author):
        logger.debug('Getting quote by author %s...' % author)
        return [q for q in self.quotes if q["author"] and q["author"].lower().find(author.lower()) >= 0]


    def get_for_keyword(self, keyword):
        logger.debug('Getting quote by keyword %s...' % keyword)
        return self.get_for_author(keyword) + \
               [q for q in self.quotes if q["quote"].lower().find(keyword.lower()) >= 0]
