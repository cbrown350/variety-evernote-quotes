# get Evernote token for production at: https://www.evernote.com/api/DeveloperToken.action
#  you may need to contact support to enable a production token at: https://www.evernote.com/SupportLogin.action
# get Evernote token for sandbox at: https://sandbox.evernote.com/api/DeveloperToken.action
EVERNOTE_USERNAME=""
EVERNOTE_TOKEN=""
QUOTES_NOTEBOOK_NAME="Quotes"
SANDBOX=False
LOG_FILE="/var/tmp/variety.EvernoteVarietyPlugin.log"
DEBUG=False
AUTHOR_DELIMITERS=['--', '―']
REFRESH_SECONDS=3600