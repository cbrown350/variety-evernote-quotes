# created using http://alexanderwaldin.github.io/packaging-python-project.html

from pip.req import parse_requirements
from pip.download import PipSession
import os, sys
from setuptools import setup, find_packages

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

def read_requirements():
    reqs_path = os.path.join(__location__, 'requirements.txt')
    install_reqs = parse_requirements(reqs_path, session=PipSession())
    reqs = [str(ir.req) for ir in install_reqs]
    return reqs

setup(name='projectname',
      version='0.1',
      description='Demo Modules',
      url='https://github.com/cbrown350/variety_evernote_quotes',
      author='The Author',
      author_email='cbrown350@gmail.com',
      license='MIT',
      packages=find_packages(),
      include_package_data=True,
      test_suite='nose.collector',
      tests_require=['nose'],
      install_requires=read_requirements())