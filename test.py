import unittest, warnings, logging, os, sys, json
import settings
from evernote.api.client import EvernoteClient
from evernote_quotes import EvernoteVarietyPlugin

unittest.TestLoader.sortTestMethodsUsing = None
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

class TestEvernoteVarietyPlugin(unittest.TestCase):    

    @classmethod
    def setUpClass(cls):
        warnings.filterwarnings("ignore", category=ResourceWarning, message="unclosed.*<ssl.SSLSocket.*>")
        warnings.filterwarnings("ignore", category=DeprecationWarning, message="inspect.getargspec().*deprecated.*")
        enp = TestEvernoteVarietyPlugin.enp = EvernoteVarietyPlugin()

        # simulate loading of the plugin, assign attributes/dirs
        class DummyObject(object):
            pass
        enp.jumble = DummyObject()
        enp.jumble.parent = DummyObject()
        enp.jumble.parent.options = DummyObject()
        enp.jumble.parent.options.quotes_max_length = 250
        enp.jumble.parent.config_folder = '/tmp/'
        enp.folder = 'test_variety_evernote_plugin'

    @classmethod
    def tearDownClass(cls):
        if TestEvernoteVarietyPlugin.enp:
            TestEvernoteVarietyPlugin.enp.deactivate()

    def test_get_info(self):
        info = EvernoteVarietyPlugin.get_info()
        print("Class info: ", json.dumps(info, indent=2))

    def test_get_evernote_info(self):
        client = EvernoteClient(token=settings.EVERNOTE_TOKEN, sandbox=settings.SANDBOX, china=False)
        userStore = client.get_user_store()
        user = userStore.getUser()
        print(user.username)
        self.assertEqual(user.username, settings.EVERNOTE_USERNAME)

        note_store = client.get_note_store()
        notebooks = note_store.listNotebooks()
        print("Found ", len(notebooks), " notebooks:")
        for notebook in notebooks:
            print(" - ", notebook.name)

    def test_activate(self):
        TestEvernoteVarietyPlugin.enp.activate()
        quotes = self.enp.quotes
        print("Loaded quotes: ", json.dumps(quotes, indent=2))
        print("Quote count was: ", len(quotes))
        self.assertGreater(len(quotes), 0)

    def test_get_random(self):
        quote = TestEvernoteVarietyPlugin.enp.get_random()
        self.assertGreater(len(quote), 0)
        print("Random quote: ", quote)

if __name__ == '__main__':
    unittest.main()
